/****************************************
	jquery Simple Box
	by Ohad Raz
	http://en.bainternet.info
****************************************/

// display the SimpleBox
function SimpleBox(insertContent, ajaxContentUrl){

	// jQuery wrapper (optional, for compatibility only)
	(function($) {
	
		// add SimpleBox/shadow <div/>'s if not previously added
		if($('#SimpleBox').size() == 0){
			var theSimpleBox = $('<div id="SimpleBox"/>');
			var theShadow = $('<div id="SimpleBox-shadow"/>');
			$(theShadow).click(function(e){
				closeSimpleBox();
			});
			$('body').append(theShadow);
			$('body').append(theSimpleBox);
		}
		
		// remove any previously added content
		$('#SimpleBox').empty();
		
		// insert HTML content
		if(insertContent != null){
			$('#SimpleBox').append(insertContent);
		}
		
		// insert AJAX content
		if(ajaxContentUrl != null){
			// temporarily add a "Loading..." message in the SimpleBox
			$('#SimpleBox').append('<p class="loading">Loading...</p>');
			
			// request AJAX content
			$.ajax({
				type: 'GET',
				url: ajaxContentUrl,
				success:function(data){
					// remove "Loading..." message and append AJAX content
					$('#SimpleBox').empty();
					$('#SimpleBox').append(data);
				},
				error:function(){
					alert('AJAX Failure!');
				}
			});
		}
		
		// move the SimpleBox to the current window top + 100px
		$('#SimpleBox').css('top', $(window).scrollTop() + 100 + 'px');
		$('#SimpleBox').css('marginLeft', '-' + $('#SimpleBox').width() / 2 + 'px');
		$('#SimpleBox-shadow').css('height', $(document).height()+'px');
		
		// display the SimpleBox
		$('#SimpleBox-shadow').fadeIn('fast', function(){
			$('#SimpleBox').fadeIn('fast');
		});
	
	})(jQuery); // end jQuery wrapper
	
}

// close the SimpleBox
function closeSimpleBox(){
	(function($) {
		
		// hide SimpleBox/shadow <div/>'s
		$('#SimpleBox').hide();
		$('#SimpleBox-shadow').fadeOut('slow');
		
		// remove contents of SimpleBox in case a video or other content is actively playing
		$('#SimpleBox').empty();
	})(jQuery); // end jQuery wrapper
}